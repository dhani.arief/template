import { BrowserRouter, Routes, Route } from "react-router-dom";
import React from "react";
import Login from "../src/pages/Login/Login";
import Dashboard from "../src/pages/Dashboard/Dashboard";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/dashboard" element={<Dashboard />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
