import React, { useState, useEffect } from "react";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Image from "react-bootstrap/Image";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import { Eye, EyeSlash } from "react-bootstrap-icons";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { LoginUser, reset } from "../../features/authSlice";
import "./Login.css";

export default function Login() {
  const [nip, setNip] = useState("");
  const [password, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { user, isError, isSuccess, isLoading, message } = useSelector(
    (state) => state.auth
  );

  useEffect(() => {
    if (user || isSuccess) {
      navigate("/dashboard");
    }
    dispatch(reset());
  }, [user, isSuccess, dispatch, navigate]);

  const Auth = (e) => {
    e.preventDefault();
    dispatch(LoginUser({ nip, password }));
  };

  const handleTogglePassword = () => {
    setShowPassword(!showPassword);
  };

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
        background: "linear-gradient(to bottom left, #d7f5fe, #0072ff",
        backgroundSize: "cover",
      }}
    >
      <Card className="card-login">
        <Container className="container-login" fluid>
          <Row style={{ height: "100%" }}>
            <Col
              style={{
                backgroundColor: "#029bff",
                padding: "2em",
              }}
              className="show-on-medium-screen"
              xs={7}
            >
              <Image
                src="https://images.squarespace-cdn.com/content/602fb923fb2e6c2319e248e3/1613741403475-TW6YFHOFE8UZFMP46534/Etana_logo.png?content-type=image%2Fpng"
                style={{
                  width: "5rem",
                  marginBottom: "60px",
                  marginTop: "2em",
                }}
              />
              <Card.Title
                style={{
                  fontSize: "3em",
                  fontWeight: "bolder",
                  color: "white",
                }}
              >
                Offboarding <br />
                Apps
              </Card.Title>
              <br />
              <Card.Text
                style={{
                  fontSize: "1em",
                  color: "white",
                }}
              >
                You can sign in to access with your <br /> existing profile
              </Card.Text>

              <div className="area">
                <ul className="circles">
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                </ul>
              </div>
            </Col>
            <Col
              className="card-sign-mobile"
              style={{
                padding: "3em",
                position: "relative",
                zIndex: "2",
                display: "flex",
                alignItems: "center",
                flexWrap: "wrap",
                alignContent: "center",
              }}
            >
              <Card.Title style={{ fontSize: "2em" }}>
                <b>Sign In</b>
                <Card.Title style={{ fontSize: "0.5em", marginTop: "2em" }}>
                  {isError ? (
                    <p>{message}</p>
                  ) : (
                    <p>Please enter your login information</p>
                  )}
                </Card.Title>
              </Card.Title>
              <Form onSubmit={Auth}>
                <Form.Group
                  as={Row}
                  controlId="formPlaintextEmail"
                  style={{ marginBottom: "8px" }}
                >
                  <Form.Control
                    type="text"
                    placeholder="E0XXX"
                    value={nip}
                    onChange={(e) => setNip(e.target.value)}
                  />
                </Form.Group>

                <Form.Group as={Row} controlId="formPlaintextPassword">
                  <div
                    className="form-control-with-button"
                    style={{ padding: "0px" }}
                  >
                    <Form.Control
                      type={showPassword ? "text" : "Password"}
                      placeholder="Password"
                      value={password}
                      onChange={(e) => setPassword(e.target.value)}
                      className="form-control-with-button"
                    />
                    <Button
                      variant="link"
                      className="btn-password-toggle"
                      onClick={handleTogglePassword}
                    >
                      {showPassword ? (
                        <EyeSlash size={20} style={{ marginTop: "-12px" }} />
                      ) : (
                        <Eye size={20} style={{ marginTop: "-12px" }} />
                      )}
                    </Button>
                  </div>
                  <p style={{ display: "flex", justifyContent: "flex-end" }}>
                    <a
                      style={{
                        color: "#036280",
                        textDecoration: "none",
                        fontSize: "12px",
                      }}
                      href="https://masteretanaapi.azurewebsites.net/forget"
                    >
                      Forgot Password ?
                    </a>
                  </p>
                </Form.Group>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    marginTop: "2em",
                  }}
                >
                  <Button
                    style={{ margin: "2px" }}
                    variant={isLoading ? "secondary" : "primary"}
                    disabled={isLoading}
                    type="submit"
                    className="button-login"
                  >
                    {isLoading ? "Loading..." : "Log In"}
                  </Button>
                </div>
              </Form>
            </Col>
          </Row>
        </Container>
      </Card>
    </div>
  );
}
