import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getMe } from "../../features/authSlice";
import { useNavigate, Navigate } from "react-router-dom";
import { Button } from "react-bootstrap";
import { LogOut, reset } from "../../features/authSlice";

export default function Dashboard() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { user, isLoading, isError } = useSelector((state) => state.auth);

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  if (isError) {
    return <Navigate to="/" />;
  }

  const logout = () => {
    dispatch(LogOut());
    dispatch(reset());
    navigate("/");
  };

  return (
    <div>
      {isLoading ? (
        "Lagi Loading"
      ) : (
        <>
          {user && user.name}
          <Button
            onClick={logout}
            variant="light"
            style={{ marginRight: "8px" }}
          >
            Log Out
          </Button>
        </>
      )}
    </div>
  );
}
