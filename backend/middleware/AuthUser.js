import { dbLogin } from "../config/Database.js";

export const verifyUser = async (req, res, next) => {
  if (!req.session.nip) {
    return res.status(401).json({ msg: "Mohon login ke akun anda" });
  }
  const [user] = await dbLogin.query(
    `SELECT * FROM MasterEmployee.dbo.MasterAccount ma 
    JOIN MasterEmployee.dbo.MasterBiodata mb ON mb.NIP = ma.NIP 
    WHERE ma.NIP = :NIP`,
    {
      replacements: { NIP: req.session.nip },
      type: dbLogin.QueryTypes.SELECT,
    }
  );
  if (!user) return res.status(404).json({ msg: "User Tidak Ditemukan" });
  req.NIP = user.NIP;
  req.role = user.role;
  next();
};

export const adminOnly = async (req, res, next) => {
  const [user] = await dbLogin.query(
    `SELECT * FROM MasterEmployee.dbo.MasterAccount ma 
    JOIN MasterEmployee.dbo.MasterBiodata mb ON mb.NIP = ma.NIP 
    WHERE ma.NIP = :NIP`,
    {
      replacements: { NIP: req.session.nip },
      type: dbLogin.QueryTypes.SELECT,
    }
  );
  if (!user) return res.status(404).json({ msg: "User Tidak Ditemukan" });
  if (user.role !== "admin")
    return res.status(403).json({ msg: "Akses Terlarang" });
  next();
};
