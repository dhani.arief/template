import express from "express";
import cors from "cors";
import session from "express-session";
import { db, dbLogin } from "./config/Database.js";
import SequelizeStore from "connect-session-sequelize";
import AuthRoute from "./routes/AuthRoute.js";

const app = express();

const sessionStore = SequelizeStore(session.Store);

const store = new sessionStore({
  db: db,
});

app.use(express.json());

app.set("trust proxy", 1);
const oneDay = 1000 * 60 * 60 * 24;
app.use(
  session({
    name: "session123",
    secret: "ssdgw4grs",
    resave: false,
    saveUninitialized: true,
    store: store,
    cookie: {
      secure: "auto",
      // httpOnly: false, //matiin kalo local
      // sameSite: "none", //matiin kalo local
      // maxAge: oneDay, //matiin kalo local
    },
  })
);

app.use(
  cors({
    credentials: true,
    origin: true,
  })
);

const PORT = process.env.PORT || 3003;

app.use(AuthRoute);

app.listen(PORT, () => {
  console.log(`Running on PORT ${PORT}`);
});
